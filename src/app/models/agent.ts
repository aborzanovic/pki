export class Agent {
	constructor(
		private username: string,
		private fullname: string,
		private agency: string,
		private social_security: string,
		private address: string,
		private phone: string
	){}
}