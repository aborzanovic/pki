export class Realestate {
	constructor(
		private id: string,
		private country: string,
		private city: string,
		private municipality: string,
		private location: string,
		private street: string,
		private number: string,
		private agent: string,
		private for_sale: boolean,
		private type: string,
		private price: string,
		private area: string,
		private owner_first_name: string,
		private owner_last_name: string,
		private phone_number: string
	){}
}