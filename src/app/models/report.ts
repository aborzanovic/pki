export class Report {
	constructor(
		private activity: string,
		private agent: string,
		private customer: string,
		private realestate: string,
		private date: string
	){}
}