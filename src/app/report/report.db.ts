export const reportsDB = [
	{
		private activity: "Kupovina",
		private agent: "agent1",
		private customer: "Miloš Milošević",
		private realestate: "Cvijićeva 100, Beograd",
		private date: "01-12-2016 17:45:52"
	},
	{
		private activity: "Prikaz",
		private agent: "agent1",
		private customer: "Miloš Milošević",
		private realestate: "Cvijićeva 101, Beograd",
		private date: "01-12-2016 17:45:52"
	},
	{
		private activity: "Prikaz",
		private agent: "agent1",
		private customer: "Nikola Nikolić",
		private realestate: "Cvijićeva 102, Beograd",
		private date: "01-12-2016 17:45:52"
	}
];