import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AgentComponent } from './agent/agent.component';
import { AgentForm } from './forms/agent/agent.form';
import { RealestateForm } from './forms/realestate/realestate.form';
import { RealestateSearchForm } from './forms/realestate/search.form';
import { RealestateComponent } from './realestate/realestate.component';
import { ReportComponent } from './report/report.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path: '', component: LoginComponent, data: { title: "" } },
  { path: 'pocetna', component: HomeComponent, data: { title: "Početna stranica" } },
  { path: 'prikaz-agenata', component: AgentComponent, data: { title: "Prikaz agenata" } },
  { path: 'dodavanje-agenata', component: AgentForm, data: { title: "Dodavanje agenata" } },
  { path: 'prikaz-nekretnina', component: RealestateComponent, data: { title: "Prikaz nekretnina" } },
  { path: 'pretraga-nekretnina', component: RealestateSearchForm, data: { title: "Pretraga nekretnina" } },
  { path: 'dodavanje-nekretnina', component: RealestateForm, data: { title: "Dodavanje nekretnine" } },
  { path: 'prikaz-izvestaja', component: ReportComponent, data: { title: "Prikaz izveštaja" } }
];

export const routing = RouterModule.forRoot(routes);
