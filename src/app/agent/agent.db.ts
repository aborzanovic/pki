export const agentsDB = [
	{
		username: "agent1",
		fullname: "Agent Jedan",
		agency: "Agencija 1",
		social_security: "123456789",
		address: "Cvijićeva 100, Beograd",
		phone: "011/46795647"
	},
	{
		username: "agent2",
		fullname: "Agent Dva",
		agency: "Agencija 1",
		social_security: "8641351384",
		address: "Cvijićeva 101, Beograd",
		phone: "011/2846843"
	},
	{
		username: "agent3",
		fullname: "Agent Tri",
		agency: "Agencija 1",
		social_security: "56843135",
		address: "Cvijićeva 102, Beograd",
		phone: "011/616843496"
	},
	{
		username: "agent4",
		fullname: "Agent Četiri",
		agency: "Agencija 1",
		social_security: "534435138",
		address: "Cvijićeva 103, Beograd",
		phone: "011/135468435"
	}
];