import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AgentComponent } from './agent/agent.component';
import { LoginComponent } from './login/login.component';
import { AgentForm } from './forms/agent/agent.form';
import { RealestateForm } from './forms/realestate/realestate.form';
import { RealestateSearchForm } from './forms/realestate/search.form';
import { RealestateComponent } from './realestate/realestate.component';
import { ReportComponent } from './report/report.component';
import { ApiService } from './shared';
import { EventBrokerService } from './shared';
import { routing } from './app.routing';

import { removeNgStyles, createNewHosts } from '@angularclass/hmr';

import { PaginationModule } from 'ng2-bootstrap';
import { Ng2TableModule } from 'ng2-table/ng2-table';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    routing,
    PaginationModule.forRoot(),
    Ng2TableModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    AgentComponent,
    LoginComponent,
    AgentForm,
    RealestateForm,
    RealestateComponent,
    RealestateSearchForm,
    ReportComponent
  ],
  providers: [
    ApiService,
    EventBrokerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(public appRef: ApplicationRef) {}
  hmrOnInit(store) {
    console.log('HMR store', store);
  }
  hmrOnDestroy(store) {
    let cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    // recreate elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // remove styles
    removeNgStyles();
  }
  hmrAfterDestroy(store) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
