import { Component, Injectable, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Realestate } from '../../models/realestate';
import { EventBrokerService } from '../../shared';

declare var jQuery: any;

@Component({
  selector: 'realestate-form',
  templateUrl: './realestate.form.html',
  styleUrls: ['./realestate.form.scss']
})
// must be an injectable because of event broker service
@Injectable()
export class RealestateForm implements OnInit {
	@Input() public realestate: Realestate;
	@Input() public isEdit: boolean;
	private editMode: boolean = false;
	private pictures: number[];

	constructor(private eventBroker: EventBrokerService) {
		this.realestate = new Realestate("", "", "", "", "", "", "", "", true, "", "", "", "", "", "");
		this.isEdit = false;
		this.pictures = [1,1,1,1,1,1,1,1,1,1,1,1];
	}

	public closeForm(): void {
		this.eventBroker.emit<boolean>("closeRealestateForm", true);
	}

	public ngOnInit(): void {
		jQuery('select').material_select();
		jQuery('ul.tabs').tabs();
	}
}
