import { Component, Injectable, Input, Output, EventEmitter, OnInit } from '@angular/core';

declare var jQuery: any;

@Component({
  selector: 'realestate-search-form',
  templateUrl: './search.form.html',
  styleUrls: ['./search.form.scss']
})
// must be an injectable because of event broker service
@Injectable()
export class RealestateSearchForm implements OnInit {
	public ngOnInit(): void {
		jQuery('select').material_select();
	}
}
