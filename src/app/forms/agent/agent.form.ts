import { Component, Injectable, Input, Output, EventEmitter } from '@angular/core';
import { Agent } from '../../models/agent';
import { EventBrokerService } from '../../shared';

@Component({
  selector: 'agent-form',
  templateUrl: './agent.form.html',
  styleUrls: ['./agent.form.scss']
})
// must be an injectable because of event broker service
@Injectable()
export class AgentForm {
	@Input() public agent: Agent;
	@Input() public isEdit: boolean;
	private editMode: boolean = false;

	constructor(private eventBroker: EventBrokerService) {
		this.agent = new Agent("", "", "", "", "", "");
		this.isEdit = false;
	}

	public closeForm() {
		this.eventBroker.emit<boolean>("closeAgentForm", true);
	}
}
