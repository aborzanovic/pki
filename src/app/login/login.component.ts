import { Component, Injectable, Output, EventEmitter } from '@angular/core';
import { EventBrokerService } from '../shared';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
// must be an injectable because of event broker service
@Injectable()
export class LoginComponent {
	private username: String = "";
	private password: String = "";

	constructor(private eventBroker: EventBrokerService){}

	submit(): void{
		this.eventBroker.emit<boolean>("loggedIn",(this.username === "admin" && this.password === "admin"));
	  }
}
