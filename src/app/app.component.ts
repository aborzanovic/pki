import { Component, Injectable } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

import { ApiService } from './shared';
import { EventBrokerService, IEventListener } from './shared';

import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

import '../style/app.scss';

declare var Materialize: any;
declare var jQuery: any;

@Component({
  selector: 'my-app', // <my-app></my-app>
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],

})
// must be an injectable because of event broker service
@Injectable()
export class AppComponent {
  private eventListener: IEventListener;
  private title: String = "Pocetna stranica";

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private eventBroker: EventBrokerService) {
    this.eventListener = this.eventBroker.listen<boolean>("loggedIn",(value:boolean)=>{
      this.logIn();
      if(value) {
        this.router.navigate(['/pocetna']);
      }
      else {
        Materialize.toast("Pogrešno korisničko ime ili šifra!", 3000, 'red-text darken-4');
      }
    });

    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .map(() => this.router.routerState.snapshot.root)
      .subscribe((event) => {
        let title = event.data ? event.data.title : "";
        while(event.firstChild !== null){
          event = event.firstChild;
          title = event.data.title;
        }
        this.title = title;

        jQuery('.collapsible').collapsible();
      });
  }

  isLoggedIn(): boolean {
    if(localStorage.getItem('loggedIn') != null){
      return localStorage.getItem('loggedIn') === "true";
    }
    else return false;
  }

  logIn() {
    localStorage.setItem('loggedIn', 'true');
  }

  logOut() {
    localStorage.setItem('loggedIn', 'false');
  }
}
